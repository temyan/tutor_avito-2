import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String s = in.nextLine();
        int c = in.nextInt();
        System.out.println(s + " " + c);
        System.out.println("Ended 1");

        c = in.nextInt(); // все методы, кроме nextLine оставляют курсор на той же строке
        // перевели в начале -> потом считали
        // nextLine -> считал сначала до конца строки и только потом перевел следующую
        in.nextLine(); // просто перевела курсор вниз
        s = in.nextLine();
        System.out.println(s + " " + c);
        System.out.println("Ended 2");


        int n = in.nextInt();
        int[] ar1 = new int[n];
        String val1 = "";
        String val2 = "";
        for (int i = 0; i < n; i++) {
            ar1[i] = in.nextInt();
            val1 = val1 + ar1[i];
        }
        int k = in.nextInt();
        ar1 = new int[k];
        for (int i = 0; i < k; i++) {
            ar1[i] = in.nextInt();
            val2 = val2 + ar1[i];
        }
        boolean flag = false;
        if (val1.length() < val2.length()) {
            System.out.println(-1);
            flag = true;
        } else if (val1.length() > val2.length()) {
            System.out.println(1);
            flag = true;
        } else {
            for (int i = 0; i < val1.length(); i++) {
                if (val1.charAt(i) - '0' < val2.charAt(i) - '0') {
                    // char - '0' -> позволяет преобразовать char к int (но не из юникода, а именно к цифре)
                    // int(char) - '0' мы не теряем что-либо
                    System.out.println(-1);
                    flag = true;
                } else if (val1.charAt(i) - '0' > val2.charAt(i) - '0') {
                    System.out.println(1);
                    flag = true;
                }
            }
        }
        if (flag == false) {
            System.out.println(0);
        }
    }
}