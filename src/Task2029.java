package en.javarush.task.pro.task06.task0608;

import java.util.Arrays;
/*
average minimum
5
1 1 2 1 1
*/

import java.util.Scanner;

public class Task2029 {
    public static void main(String arg[]) {
        System.out.println("Введите длину массива");
        Scanner in = new Scanner(System.in);
        //длина массива
        int len = in.nextInt();
        //основной массив
        int[] array = new int[len];
        //принимаем любые целые числа в основной массив array
        System.out.println("Введите любых пять целых чисел");
        for (int i = 0; i < array.length; i++) {
            Scanner in2 = new Scanner(System.in);
            System.out.println("Введите еще " + (len - i));
            int position = in2.nextInt();
            array[i] = position;
        }

        //Создаем новый массив, копируем данные в него и сортируем его для определения минимального числа, что бы не нарушать порядок в основном.
        int[] min_array = new int[array.length];
        for (int i = 0; i < min_array.length; i++) {
            min_array[i] = array[i];
        }
        //Arrays.sort(min_array);

        int ind = 0;
        for (int a = 1; a <= min_array.length - 1; a++) {
            int help;
            if (min_array[ind] > min_array[a]) {
                help = min_array[ind];
                min_array[ind] = min_array[a];
                min_array[a] = help;
            }
            if (a == min_array.length - 1) {
                ind++;
                if (ind != a) {
                    a = ind;
                } else {
                    break;
                }
            }
        }
        int min = min_array[0];

        //узнаем количество повторяющихся позиций
        int count = 0;
        for (int j = 0; j < array.length; j++) {
            if (array[j] == min) {
                count++;
            }
        }

        //создаем новый массив ,затем вкладываем туда номера позиций ( не индексы ) повторяющихся минимальных чисел
        int[] new_array = new int[count];
        short t = 0;//вспомогательный счетчик
        for (int a = 0; a < array.length; a++) {
            if (array[a] == min) {
                new_array[t] = a;
                t++;
            }
        }

        float len_new = new_array.length;
        if (len_new % 2 == 0) {
            //определяем ИНДЕКС минимального числа в новом массиве при четном колличестве позиций
            // получается индекс из нового массива который нам указывает на индекс из основного массива
            int x = (int) len_new / 2 - 1;
            int a = new_array[x];
            //Позиция нужного элемента = индекс основного массива + 1
            //System.out.println(Arrays.toString(new_array));
            //System.out.println(Arrays.toString(array));
            String str = "";
            for (int i = 0; i < array.length; i++) {
                str = str + array[i] + " ";
            }
            System.out.println(str);
            System.out.println("Минимальная позиция из двух средних минимумов в основном массиве: " + (a + 1));
        } else {
            float x = (Math.round(len_new / 2));
            int y = (int) x - 1;
            int a = new_array[y];
            //System.out.println(Arrays.toString(new_array));
            //System.out.println(Arrays.toString(array));
            String str = "";
            for (int i = 0; i < array.length; i++) {
                str = str + array[i] + " ";
            }
            System.out.println(str);
            System.out.println("Позиция среднего минимума в основном массиве: " + (a + 1));


        }


    }


}