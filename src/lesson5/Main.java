package lesson5;

import java.util.Arrays;

public class Main {
    // Строки - продолжение
    public static int k = 123; // default

    static void qwe() { // default модификатор
        System.out.println(k);
    }

    public static void main(String[] args) {
        String s = "Hel2lo   aaaaaaaaaaaaaaaW999or312ld                         He5llo  Hel11lo Wo2rldHello";
        System.out.println(s.substring(2, 4)); // возвращает подстроку начиная с 2 позиции и заканчивая 4 (не включая символ под позицией 4)
        System.out.println(s.substring(3)); // возвращает подстроку начиная с 3 позиции и заканчивая s.length()

        System.out.println(s.replaceAll("", ""));
        // [выражение1]|[выражение2] -> логическое ИЛИ
        // \\s+ -> этот плюс - любое количество пробелов идущих подряд
        // [^0-9] -> все, кроме 0-9
        // RegEx -> Regular Expression -> регулярные выражения -> специальный синтаксис, позволяющий по определенным параметрам "разбирать" строку
        // Как выглядит RegEx:
        // \\s (literal) ...
        // [0-9] -> то, что записано в квадратных скобках и имеющее между собой - это промежуток ([0-9] - промежуток от 0 до 9 включительно
        // [а-яА-ЯёЁ] -> все буквы русского алфавита, ё выделил отдельно, так как эта буква лежит в таблице Unicode дальше, чем русский алфавит а-я
        // Как строка ("Hello")

        // Что такое литерал?
        // Литерал -> специальные символы
        System.out.println("Hello");
        System.out.println("World");
        System.out.println("Hello World"); // \ -> показывает, что в данном месте у нас литерал
        // Основные литералы:
        // \n -> перевод на следующую строку
        // \b -> backspace
        // \s -> whitespace (пробел)
        // \t -> табуляция (кнопка tab, Обычно три пробела, но количество зависит от контекста)
        // с помощью литералов можно обособлять какие-то символы (например, кавычки \" или \\)

        // split("regex") -> позволяет разбить строку по какому-либо признаку на массив строк
        String[] ar = s.split("[\\s]+|[0-9]+"); // Pattern
        System.out.println(Arrays.toString(ar));
        // todo http://acm.sgu.ru/lang/problem.php?contest=2&problem=2036
        // todo http://acm.sgu.ru/lang/problem.php?contest=2&problem=2039
    }
}
