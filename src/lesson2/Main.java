package lesson2;

import java.util.Scanner;

public class Main {
    // if, for
    public static void main(String[] args) {
        int a = 11;
        // Dangling if
        // без if ... { } -> выполняется только первая строка кода
        if (a == 10) {  // if (*выражение1* && *выражение2*...) true
//            System.out.println("1");
//            System.out.println("2");
//            System.out.println("3");
//            System.out.println("4");
//            System.out.println("5");
//            System.out.println("6");
        } else if (a == 11) {
//            System.out.println("lo");
//            System.out.println("lo");
        } else {
//            System.out.println("qwewqe");
        }

        // Логические операнды (в контексте условий (if) )

        // && - Логическое И (выражение1 (false) && выражение2) -> Lazy
        // || - Логическое ИЛИ (выражение1 (true) || выражение2) -> Lazy
        // & - Логическое И (выражение1 (false) И выражение2) смотрим до конца
        // | - Логическое ИЛИ (выражение1 (true) || выражение2)
        // !(выражение1) -> отрицание
        // if (!(x >= 5)) -> if (x < 5)

        // В контексте операций
        // & (7 & 2) -> Логическое И (из мира Дискретной математики) -> (111 & 010) -> 010 -> 2
        // | (7 | 2) -> (111 | 010) -> 111...
        // ^ -> Исключающее ИЛИ (xor) -> (111 ^ 010) -> (101)
        // >>>... сдвиг побитовый
        int b = 7 ^ 2;

        // Основные операции
        // +, -, *, / - div, % - mod
        int i = 7 / 2; // 3
        int p = 7 % 2; // 1
        int x = 62 % 68; // Если левое число меньше правого, то первое число полностью вмещается во второе (в ответе будет просто первое число)
        // 0 67/100
        // 0 2/5
        // 0 62/68
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        // Знаки сравнения
        // >, <, >=, <= (дробные, целые)
        // ==, !=
        //System.out.println(x);
        if (number > 13 && number <= 25) { // [14...25]
            // 15 % 2 => 1
            // 2516 % 2 => 0

            // 0 1

            // 15 % 3 => 0
            // 16 % 3 => 1
            // 17 % 3 => 2
            // 18 % 3 => 0
            // 0 1 2

            // 15 % 4 => 3
            // 16 % 4 => 0
            // 0 1 2 3
            // % 7
            // 0 1 2 3 4 5 6
            if (number % 2 != 0) {
                System.out.println("Nechet");
            } else {
                System.out.println("Chet");
            }
        }
        // Java ~12+
        // switch (number)
        switch (number) {
            case 1: // if (number == 1) false
                System.out.println("Январь");
                break;
            case 2: // true
                System.out.println("Февраль");
                break;
            case 3: // true
                System.out.println("Март");
                break;
            case 4: // true
                System.out.println("Апрель");
                break;
            case 5:
                System.out.println("Май");
                break;
            case 6:
                System.out.println("Июнь");
                break;
            case 7:
                System.out.println("Июль");
                break;
            case 8:
                System.out.println("Август");
                break;
            case 9:
                System.out.println("Сентябрь");
                break;
            case 10:
                System.out.println("Октябрь");
                break;
            case 11:
                System.out.println("Ноябрь");
                break;
            case 12:
                System.out.println("Декабрь");
                break;
            default:
                System.out.println("Такого месяца нет");
                break;
        }
        for (int k = 0; k < 100; k++) {
            for (int j = 0; j < 100; j++) {
                System.out.println(j);
                if (j > 15) {
                    if (j % 2 == 0) {
                        break;
                    }
                }
            }
        }
        for (int j = 0; j < 10; j++) {
            if (j % 2 == 0) {
                continue; // переводит на следующую итерацию только ближайший цикл
            }
            // ...
            System.out.println(j);
        }

        // break; -> в основном используется в циклах (потому что break прерывает текущий цикл)
        // continue; -> используется в циклах для того, чтобы пропустить итерацию

        // Циклы в Java
        // while первый цикл в ЯП в принципе
        // for считается синтаксическим сахаром
        // for <-> while
        int v = in.nextInt();
        while (v > 0 && v % 2 == 0) { // до тех пор, пока v > 0 И v четное
//            if (v % 2 != 0) { // если v нечетное - закончи
//                break;
//            }
            System.out.println(v);
            v = v - 1;
        }
        boolean flag = false;
        if (!flag) { // if (flag == false)
            System.out.println(false);
        } else if (flag) { // if (flag) то же самое, что if (flag == true)
            System.out.println(true);
        }
        // Stack
        // Front-end: HTML + CSS, JavaScript (VUE.js, Angular.js), TypeScript... Java FX, Java Swing, JAVA GUI
        // Back-end: Spring Framework, Quarkus Framework, Hibernate Framework, Nosql/Sql...
    }
}
