import java.util.Arrays;
import java.util.Scanner;

public class Task2032 {
    public static void main(String[] args) {
        //вводим число цифр для определения длины первого массива
        // 2
        // 9 8
        // 10000
        // 9 9 9 9 9..... 9....
        Scanner in = new Scanner(System.in);
        int len1 = 0; // in.nextInt();
        int len2 = 0;
        if (in.hasNextInt()) {
            len1 = in.nextInt();
        } else {
            System.out.println("Вы ввели не число! Попробуйте заного");
        }
        //Создаем первый массив
        int[] array1 = new int[len1];
        //for - через пробел присваиваем цифры в первый массив массив
        for (int i = 0; i < len1; i++) {
            //создаем второй ввод данных для ввода чисел массива
            System.out.println("Введите числа в массив 1");
            int numbers = in.nextInt();
            array1[i] = numbers;

        }
        System.out.println(Arrays.toString(array1));
        //складываем все числа массива и создаем число
        String str = "";
        for (int i = 0; i < array1.length; i++) {
            str = str + array1[i];
        }
        //Получаем первое число
        int num1 = Integer.parseInt(str); // Сломается, если числа будут большими
        //
        System.out.println(num1);


        //Второй массив
        System.out.println("Второй массив");
        if (in.hasNextInt()) {
            len2 = in.nextInt();
        } else {
            System.out.println("Вы ввели не число! Попробуйте заного");
        }
        int[] array2 = new int[len2];
        for (int i = 0; i < len2; i++) {
            System.out.println("Введите числа в массив 2");
            int numbers = in.nextInt();
            array2[i] = numbers;
        }
        System.out.println(Arrays.toString(array2));
        String str2 = "";
        for (int i = 0; i < array2.length; i++) {

            str2 = str2 + array2[i];
        }
        //Получаем первое число
        int num2 = Integer.parseInt(str2);

        //сравниваем два полученных числа по условию
        if (num1 < num2) {
            System.out.println(-1);
        } else if (num1 == num2) {
            System.out.println(0);
        } else {
            System.out.println(1);
        }
        in.close();

    }

}