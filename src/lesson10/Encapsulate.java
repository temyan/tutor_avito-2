package lesson10;

import java.util.Scanner;

public class Encapsulate {
    public static void main(String[] args) {
        User u = new User("Sergey", 19, "blsa");
        u.show();
//        u.setAge(-100);
//        u.show();
        System.out.println(u.getAge());
        System.out.println(u.getPassword());
    }
}

class User {
    // Инкапсуляция - это сокрытие чего-либо. Это пометка private на поля или методы
    private String name;
    private int age;
    private String password;

    public User(String name, int age, String password) {
        this.name = name;
        setAge(age);
        setPassword(password);
//        this.age = age;
    }
    // Getter & Setter -> обычные методы, getter - просто возвращает значение, setter - просто ставит значение (на поле)


    public String getName() { // Getter
        // private -> public
        return name;
    }

    public void setName(String name) { // Setter
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0 || age > 100) {
            System.out.println("Неправильные данные");
            System.exit(0);
        }
        this.age = age;
    }


    public String getPassword() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите ваше имя для подтверждения личности");
        String s = in.next();
        if (!s.equals(name)) {
            return "Доступ отказан!";
        } else {
            return password;
        }
    }

    public void setPassword(String password) {
        if (password.length() < 4) {
            System.out.println("Ваш пароль должен быть длиннее!");
            System.exit(0);
        }
        this.password = password;
    }

    public void show() {
        System.out.println("Имя: " + name + ", возраст: " + age + ", пароль: " + password);
    }
}
