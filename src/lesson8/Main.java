package lesson8;

public class Main {
    // Основные принципы ООП:
    // 1) Инкапсуляция
    // 2) Наследование - способность одного класса полностью перенимать другой класс (поля, методы)
    // A extends B
    // 3) Полиморфизм
    // 4) Абстракция
    public static void main(String[] args) {
        Person p = new Person();
        Student st = new Student(); // Создание объекта
        // Создание экземпляра класса;
        // правая часть вызывает в классе Student специальный метод - конструктор
        st.whoamI();
        st.mark();
        p.whoamI();
        System.out.println(st.age);
        System.out.println(st.nameOfPerson());
        Student2 st2 = new Student2();
        System.out.println(st2.nameOfPerson());

        Employee e = new Employee("Jack", 21);
        System.out.println(e.name);
        System.out.println(e.age);
        // В Java нельзя наследоваться одновременно от нескольких классов
    }
}

class PersonMain {
    String name = "Oleg";
    int age = 55;
}

class Person extends PersonMain {
    String name = "Sergey";
    int age = 19;

    public void whoamI() {
        System.out.println("I am Person");
    }

    public String getPersonMainName() {
        return super.name;
    }
}

class Employee extends Person {
    String name = "Victor";
    int age = 25;

    public Employee(String name, int age) { // конструктор
        // name = name; // Java не понимает, что слева мы хотим использовать поле, а справа то, что пришло
        this.name = name; // присвоит имя которое пришло к нам к полю
        this.age = age;
    }


    public Employee() { // Пустой конструктор
        // Перегрузка метода

    }


    public void hello() {
        System.out.println("Hello");
    }

    public String nameOfPerson() {
        return getPersonMainName(); // name от Person
        // super - ключевое слово, которое позволяет обращаться к чему либо в родительском классе
        // обращение к родительскому конструктору происходит через super(*параметры конструктора*)
        // противоположность super -> this.
    }
}

class Student extends Employee {
    // Конструктор - специальный метод призванный инициализировать будущий объект
    // Входная точка для взаимодействия с полями и методами данного класса
    // конструктор всегда существует
    // По умолчанию такой конструктор всегда пустой. Его сигнатура совпадает с названием класса
    public Student() {
//        super("asd", 10);
    }

    public void mark() {
        System.out.println("5");
    }
}

class Student1 extends Student {

}

class Student2 extends Student1 {

}

class Student3 extends Student2 {

}
